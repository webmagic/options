<?php

namespace Webmagic\Options;


use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Webmagic\Options\Counters\CountersManager;

class OptionsServiceProvider extends ServiceProvider
{
    /**
     * Register services
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/options.php', 'webmagic.options'
        );


        $this->app->bind(CountersManager::class, function () {
           $counters_path = config('webmagic.options.counters_file_path');
           return new CountersManager($counters_path);
        });
    }


    /**
     * Bootstrapping services
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        //Config publishing
        $this->publishes([
            __DIR__ . '/config/options.php' => config_path('webmagic/options.php')
        ], 'notifier-config');

        $this->registeringMiddleware($router);

    }



    /**
     * Middleware registration
     *
     * @param $router
     */
    protected function registeringMiddleware($router)
    {
        $router->middlewareGroup('options', [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
        ]);
    }
}