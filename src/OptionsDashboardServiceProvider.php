<?php

namespace Webmagic\Options;


use Illuminate\Routing\Router;
use Webmagic\Dashboard\Dashboard;


class OptionsDashboardServiceProvider extends OptionsServiceProvider
{
    /**
     * Register pages service
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__ . '/config/options_dashboard.php', 'webmagic.dashboard.options'
        );
    }

    /**
     * Boot pages service
     * @param Router $router
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $this->loadTranslation();

        //Routs registering
        $this->loadRoutesFrom(__DIR__.'/Http/routes_dashboard.php');

        //Config publishing
        $this->publishes([
            __DIR__ . '/config/options_dashboard.php' => config_path('webmagic/dashboard/options.php'),
        ], 'config');

        //Publish translations
        $this->publishes([
            __DIR__.'/resources/lang/' => resource_path('lang/vendor/options'),
        ]);

        //Add items menu
        $this->includeMenuForDashboard();


    }

    /**
     * Including menu items for new dashboard
     */
    protected function includeMenuForDashboard()
    {
        if(!$menu_item_config = config('webmagic.dashboard.options.dashboard_menu_item')){
            return;
        }

        //Add menu into global menu
        $dashboard = app()->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($menu_item_config);
    }


    /**
     * Load translation
     */
    public function loadTranslation()
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'options');
    }

}