<?php


namespace Webmagic\Options\Counters;


use Illuminate\Support\Facades\View;
use Webmagic\Options\Counters\Exception\CountersFileNotFoundException;

class CountersManager
{
    /**
     * Path to counters blade
     * May be:
     * - path in view path format
     * - full path to file
     * - path to file based on view base path
     *
     * @var string
     */
    protected $counters_template_path;

    /**
     * CountersManager constructor.
     *
     * @param string $counters_template_path
     */
    public function __construct(string $counters_template_path)
    {
        $this->counters_template_path = $counters_template_path;
    }

    /**
     * Return string with current counters
     */
    public function getCounters(): string
    {
        $path = $this->getFilePath();
        $counters_string = file_get_contents($path);

        return $counters_string;
    }

    /**
     * Update counters file
     *
     * @param string $counters_string
     */
    public function saveCounters(string $counters_string)
    {
        $path = $this->getFilePath();
        file_put_contents($path, $counters_string);
    }

    /**
     * Return counters file path
     *
     * @return string
     * @throws CountersFileNotFoundException
     */
    protected function getFilePath()
    {
        if (!view()->exists($this->counters_template_path)) {
            throw new CountersFileNotFoundException($this->counters_template_path);
        }

        return view($this->counters_template_path)->getPath();
    }

}