<?php


namespace Webmagic\Options\Counters\Exception;


class CountersFileNotFoundException extends \Exception
{

    /**
     * CountersFileNotFoundException constructor.
     *
     * @param string $used_path
     */
    public function __construct($used_path)
    {
        parent::__construct("Counters file not found in $used_path");
    }
}