<?php

return [

   /*
   |--------------------------------------------------------------------------
   | Dashboard menu settings
   |--------------------------------------------------------------------------
   |
   */
    'dashboard_menu_item' => [
        'text' => 'options::common.options',
        'icon' => 'fa-cog',
        'rank' => 10,
        'link' => 'dashboard/options',
        'active_rules' => [
            'urls' => 'dashboard/options'
        ]
    ]
];