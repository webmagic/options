<?php

Route::group([
    'as' => 'options::',
    'prefix' => 'dashboard/options',
    'middleware' => ['options', 'auth'],
    'namespace' => '\\Webmagic\\Options\\Http\\Controllers'
], function (){

    Route::get('/', [
        'as' => 'index',
        'uses' => 'Controller@index'
    ]);

    Route::post('counters', [
        'as' => 'counters-update',
        'uses' => 'Controller@countersUpdate'
    ]);
});