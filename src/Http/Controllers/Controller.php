<?php


namespace Webmagic\Options\Http\Controllers;


use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Options\Counters\CountersManager;

class Controller extends \Illuminate\Routing\Controller
{
    use ValidatesRequests;

    /**
     * Show main page
     *
     * @param CountersManager $countersManager
     *
     * @return mixed
     */
    public function index(CountersManager $countersManager)
    {
        $counters = $countersManager->getCounters();


        $formPageGenerator = (new FormPageGenerator())
            ->title(__('options::common.options'))
            ->action(route('options::counters-update'))
            ->method('POST')
            ->ajax(true)
            ->textarea('counters', isset($counters) ? $counters : null, __('options::common.counters'), false)
            ->submitButtonTitle(__('options::common.save'))
        ;

        return $formPageGenerator;
    }

    /**
     * Save counters
     *
     * @param Request         $request
     * @param CountersManager $countersManager
     */
    public function countersUpdate(Request $request, CountersManager $countersManager)
    {
        $counters_value = '';

        if ($request->has('counters') && $request->counters) {
            $counters_value = $request->counters;
        }

        $countersManager->saveCounters($counters_value);
    }
}